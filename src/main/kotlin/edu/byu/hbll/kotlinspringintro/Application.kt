package edu.byu.hbll.kotlinspringintro

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Application

/**
 * Main method; to compile and run this program, run the following from the project's root directory:
 *
 * mvn clean package
 * java -jar target/kotlin-spring-intro-0.1.0.jar
 */
fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
